<?php

namespace Anzahtools\EmailConfirmTimeRemove\Cron;

class CheckUnconfirmUser
{
  public static function run()
  {
		$options = \XF::options();

    $at_ectr_time = $options->at_ectr_time;
    $at_ectr_log = $options->at_ectr_log;
    $at_ectr_criteria = array_values($options->at_ectr_criteria);


    $at_ectr_states = array_values($options->at_ectr_state); //array enthält email_confirm, email_confirm_edit, email_bounce
    $at_etcr_possvalues = ["email_confirm","email_confirm_edit","email_bounce"];
    $at_ectr_finalStates = [];

    for($i =0;$i<3;$i++ )
    {
      if($at_ectr_states[$i]=='1')
      {
        array_push($at_ectr_finalStates, $at_etcr_possvalues[$i]);
      }
    }

    $users = \XF::finder('XF:USER')->where([
        ['register_date', '<=' ,time() - 86400 * $at_ectr_time]
      ])->fetch();

      \XF::dump($at_ectr_criteria);

    foreach($users as $user)
    {
      if (in_array($user->user_state, $at_ectr_finalStates))
  		{
        if(!$user->is_banned && $user->message_count == 0 || $user->is_banned && $at_ectr_criteria[0] && $user->message_count == 0 || !$user->is_banned && $user->message_count >= 0 && $at_ectr_criteria[1] || $user->is_banned && $at_ectr_criteria[0] && $user->message_count >= 0 && $at_ectr_criteria[1]  )
        {
          $user->delete();
        }
  		}

    }

  }

}
